﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AngularTest.Models;
using System.Web.Http.Description;

namespace AngularTest.Controllers
{
    public class GetImageDetailsController : ApiController
    {

        private GetImageDetailsModels[] imgDetails = new GetImageDetailsModels[]
             {
            new GetImageDetailsModels { ImageType="WEB",Resolution="720PI",Dimensions="540*740",Size="3'*3'",Price="500",Discount="10",NetPrice="450" },
            new GetImageDetailsModels { ImageType="LOW",Resolution="1500PI",Dimensions="1540*740",Size="3'*2'",Price="550",Discount="10",NetPrice="500" },
            new GetImageDetailsModels { ImageType="MEDIUM",Resolution="480PI",Dimensions="240*240",Size="3'*5'",Price="750",Discount="10",NetPrice="700" },
            new GetImageDetailsModels {ImageType="HIGH",Resolution="240PI",Dimensions="120*120",Size="3'*8'",Price="1500",Discount="10",NetPrice="1450" },
             };



        // GET: api/GetImageDetails
        [ResponseType(typeof(IEnumerable<GetImageDetailsModels>))]
        public IEnumerable<GetImageDetailsModels> GetImageDetails()
        {
            return imgDetails;
        }

        // GET: api/GetImageDetails/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/GetImageDetails
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/GetImageDetails/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/GetImageDetails/5
        public void Delete(int id)
        {
        }
    }
}
