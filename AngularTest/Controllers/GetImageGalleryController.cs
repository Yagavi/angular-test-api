﻿using AngularTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace AngularTest.Controllers
{
    public class GetImageGalleryController : ApiController
    {

        private ImageGalllery[] img = new ImageGalllery[]
        {


            new ImageGalllery {ID=1, URL=@"/assets/Images/Food.jpg",name="food"},
            new ImageGalllery {ID=2, URL=@"/assets/Images/Hotels.jpg",name="Hotels"},
            new ImageGalllery {ID=3, URL=@"/assets/Images/dresses.jpg",name="Dresses"},
            new ImageGalllery {ID=4, URL=@"/assets/Images/furniture.jpg",name="Furniture"},
        };


        private ImageGalllery[] imgbyid = new ImageGalllery[]
               {
            new ImageGalllery { ID = 2, URL = @"/assets/Images/Food1.jpg", name = "food" },
            new ImageGalllery { ID = 2, URL = @"/assets/Images/Food2.jpg", name = "food" },
            new ImageGalllery { ID = 2, URL = @"/assets/Images/Food3.jpg", name = "food" },
            new ImageGalllery { ID = 2, URL = @"/assets/Images/Food4.jpg", name = "food" },
               };


      
        // GET: api/GetImageGallery

        [ResponseType(typeof(IEnumerable<ImageGalllery>))]
        public IEnumerable<ImageGalllery> Get()
        {
            return img;
        }

        [ResponseType(typeof(IEnumerable<ImageGalllery>))]
        // GET: api/GetImageGallery/5
        public IEnumerable<ImageGalllery> Get(int id)
        {
            return imgbyid;
        }

// POST: api/GetImageGallery
public void Post([FromBody]string value)
{
}

// PUT: api/GetImageGallery/5
public void Put(int id, [FromBody]string value)
{
}

// DELETE: api/GetImageGallery/5
public void Delete(int id)
{
}
    }
}
