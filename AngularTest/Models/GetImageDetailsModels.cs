﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularTest.Models
{
    public class GetImageDetailsModels
    {
        public string ImageType { get; set; }
        public string Resolution { get; set; }
        public string Dimensions { get; set; }
        public string Size { get; set; }
        public string Price { get; set; }
        public string Discount { get; set; }
        public string NetPrice { get; set; }
    }
}